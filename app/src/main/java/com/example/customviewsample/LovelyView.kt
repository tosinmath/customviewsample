package com.example.customviewsample

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;
import android.R
import com.example.customviewsample.LovelyView



class LovelyView : View {

    //circle and text colors
    private var circleCol: Int = 0
    private var labelCol: Int = 0
    //label text
    private val circleText: String? = null
    //paint for drawing custom view
    private var circlePaint: Paint? = null

    // Invalidate to Re-draw the view

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        //paint object for drawing in onDraw
        circlePaint = Paint()

        //get the attributes specified in attrs.xml using the name we included
        val a = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.LovelyView, 0, 0
        )

        try {
            //get the text and colors specified using the names in attrs.xml
            circleText = a.getString(R.styleable.LovelyView_circleLabel);
            circleCol = a.getInteger(R.styleable.LovelyView_circleColor, 0);//0 is default
            labelCol = a.getInteger(R.styleable.LovelyView_labelColor, 0);
        } finally {
            a.recycle();
        }
    }

    override fun onDraw(canvas: Canvas) {
        //draw the View

        //get half of the width and height as we are working with a circle
        val viewWidthHalf = this.measuredWidth / 2
        val viewHeightHalf = this.measuredHeight / 2

        var radius = 0
        if (viewWidthHalf > viewHeightHalf) {
            radius = viewHeightHalf - 10
        } else {
            radius = viewWidthHalf - 10
        }

        circlePaint.setStyle(Style.FILL);
        circlePaint.setAntiAlias(true);

        //set the paint color using the circle color specified
        circlePaint.setColor(circleCol);

        canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, circlePaint);

        //set the text color using the color specified
        circlePaint.setColor(labelCol);

        //set text properties
        circlePaint.setTextAlign(Paint.Align.CENTER);
        circlePaint.setTextSize(50);

        //draw the text using the string attribute and chosen properties
        canvas.drawText(circleText, viewWidthHalf, viewHeightHalf, circlePaint);
    }


    fun getCircleColor(): Int {
        return circleCol
    }

    fun getLabelColor(): Int {
        return labelCol
    }

    fun getLabelText(): String {
        return circleText
    }

    fun setCircleColor(newColor: Int) {
        //update the instance variable
        circleCol = newColor
        //redraw the view
        invalidate()
        requestLayout()
    }

    fun setLabelColor(newColor: Int) {
        //update the instance variable
        labelCol = newColor

        //redraw the view
        invalidate()
        requestLayout()
    }

}